import { Skill } from "./Skill";

export class User {
  id: number;
  login: string;
  password: string;
  email: string;
  firstName: string;
  lastName: string;
  registrationDate: Date;
  country: string;
  city: string;

  constructor() {

  }
}
