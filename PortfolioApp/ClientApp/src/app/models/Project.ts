import { Tag } from "./Tag";
import { Language } from "./Language";

export class Project {
  id: number;
  name: string;
  description: string;
  tags: Tag[];
  languages: Language[];
  url: string;
  ownerId: number;
  publishDate: Date;

    constructor() {

    }
  }
