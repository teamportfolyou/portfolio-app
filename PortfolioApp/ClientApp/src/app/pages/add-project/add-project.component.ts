import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/models/Project';
import { Http } from '@angular/http';
import { PortfolioAppService } from 'src/app/services/portfolio-app.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Language } from 'src/app/models/Language';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { TagInputModule } from 'ngx-chips';
import { FormControl } from '@angular/forms';
import { NotifierService } from "angular-notifier";
import { Observable, Subject } from 'rxjs';
import { of } from 'rxjs';

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.css']
})
/** add-project component*/
export class AddProjectComponent implements OnInit {
  newProject: Project = new Project;
  userId: number;
  languagesList: Language[];
  newLanguages: Language[];
  selectedLanguage: Language;
  dropdownList = [];
  selectedItems = [];
  dropdownSettings: IDropdownSettings;
  public validators = [this.minimumOfCharacters];
  public errorMessages = {
    'minimumOfCharacters': 'Your tag must be minimum of 2 characters'
  };
  private readonly notifier: NotifierService;


  /** add-project ctor */
  constructor(private http: Http, notifierService: NotifierService,
    private portfolioAppService: PortfolioAppService, private route: ActivatedRoute, private router: Router) {

    this.notifier = notifierService;
  }

  ngOnInit() {
    console.log("ngOnInit started");
    this.userId = this.route.snapshot.params['id'];

    //dropdown initialization
    this.newProject.languages = new Array<Language>();
    this.getLanguagesList();
    this.dropdownList = this.languagesList;
    this.dropdownSettings = {
      singleSelection: false,
      textField: 'value',
      selectAllText: 'Select all',
      unSelectAllText: 'Unselect all',
      itemsShowLimit: 4,
      allowSearchFilter: true,
      enableCheckAll: false,
      clearSearchFilter: true
    };

  }

  getLanguagesList() {
    this.portfolioAppService.getLanguagesList().subscribe(
      (data: Language[]) => {
        this.languagesList = data;
      }
    )
  }

  addProject() {
    let date: Date = new Date();
    this.newProject.ownerId = this.userId;
    debugger;
    if (this.newLanguages && this.newLanguages.length > 0) {
      this.newLanguages.forEach(x => {
        this.newProject.languages.push(new Language(x.value));
      });
      this.newProject.publishDate = date;
      this.portfolioAppService.addProject(this.newProject).subscribe(
        (data: any) => {
          this.clearFields();
          this.navigateToProfile(this.userId);
        },
        error => console.error(error)
      );
    }
    else {
      this.notifier.notify("error", "You must select minimum of 1 project language");
    }
  }

  navigateToProfile(id: number) {
    this.router.navigate(['/profile', id]);
  }

  clearFields() {
    this.newProject = new Project();
    this.newLanguages = null;
  }

  private minimumOfCharacters(control: FormControl) {
    if (control.value.length < 2) {
      return {
        'minimumOfCharacters': true
      };
    }
    return null;
  }

  onItemSelect(item: any) {
    this.notifier.notify("success", "Language added");
  }

  onSelectAll(items: any) {
    //console.log(this.newProject.languages);
  }

  onCancel() {
    this.router.navigate(['/profile', this.userId]);
  }

  transform(value: string): Observable<object> {
    const item = { display: `#${value}`, value: `#${value}` };
    return of(item);
  }

}
