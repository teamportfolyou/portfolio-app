import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { AddProjectComponent } from './add-project.component';

let component: AddProjectComponent;
let fixture: ComponentFixture<AddProjectComponent>;

describe('add-project component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ AddProjectComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(AddProjectComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});
