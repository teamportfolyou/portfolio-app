import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/User';
import { Router, ActivatedRoute } from '@angular/router';
import { Project } from 'src/app/models/Project';
import { PortfolioAppService } from 'src/app/services/portfolio-app.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';

@Component({
    selector: 'app-edit-profile',
    templateUrl: './edit-profile.component.html',
    styleUrls: ['./edit-profile.component.scss']
})
/** edit-profile component*/
export class EditProfileComponent {

  @Input() userInfo: User;
  userId: number;
  projectsList: Project[] = [];

    /** edit-profile ctor */
  constructor(private route: ActivatedRoute, private portfolioAppService: PortfolioAppService, private router: Router) {

  }

  ngOnInit() {
    this.userId = this.route.snapshot.params['id'];
    this.getAllProjectsByUserId(this.userId);
  }

  getAllProjectsByUserId(id: number) {
    this.portfolioAppService.getAllProjectsByUserId(id).subscribe((data) => {
      this.projectsList = data;
      console.log(this.projectsList);
    },
      error => console.error(error)
    );
  }

  getSkillsByUserId(id: number) {
    this.portfolioAppService.getAllProjectsByUserId(id).subscribe((data) => {
      this.projectsList = data;
      console.log(this.projectsList);
    },
      error => console.error(error)
    );
  }
}
