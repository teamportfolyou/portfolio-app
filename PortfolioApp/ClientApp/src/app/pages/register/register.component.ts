import { Component, OnInit, ViewChild } from '@angular/core';
import { Http, Response } from '@angular/http';
import { User } from 'src/app/models/User';
import { Observable, Subject } from 'rxjs';
import { catchError, retry, take } from 'rxjs/operators';
import { PortfolioAppService } from 'src/app/services/portfolio-app.service';
import { FormGroup, FormControl, NgForm, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
/** register component*/
export class RegisterComponent implements OnInit {
  loginInput: string;
  passwordInput: string;
  emailInput: string;
  repeatPasswordInput: string;
  firstnameInput: string;
  lastnameInput: string;
  errorMessage = '';
  newUser: User = new User;
  invalidRegister = false;
  passwordMatchStatus: boolean = true;
  emailStatus: boolean;

  @ViewChild('f') signupForm: NgForm;

  /** register ctor */
  constructor(private http: Http, private portfolioAppService: PortfolioAppService, private router: Router) {

  }

  ngOnInit(): void {
    console.log("on init works");

  }

  registerUser(form: NgForm) {
    let date: Date = new Date();
    this.newUser.registrationDate = date;
    this.checkEmailStatus().pipe(take(1)).subscribe(status => {  // <-- take first returned variable
      if (this.signupForm.valid === true && status) {  // <-- check the status of email address
        this.portfolioAppService.registerUser(this.newUser).subscribe((data) => {
          this.clearFields();
          this.navigateToLogin();
        },
          error => console.error(error)
        );
      }
    });
  }

  checkEmailStatus(): Observable<boolean> {
    const result = new Subject<boolean>();
    this.portfolioAppService.checkEmailStatus(this.newUser.email).subscribe(
      (data: string) => {
        if (data !== '') {
          result.next(true);
        }
        else result.next(false);
      },
      error => {
        console.error(error);
        result.next(false);
      }
    );

    return result.asObservable();
  }

  checkPassword() {
    if (this.newUser.password) {
      if (this.repeatPasswordInput === this.newUser.password) {
        this.passwordMatchStatus = true;
      }
      else this.passwordMatchStatus = false;
    }
  }

  clearFields() {
    this.newUser.lastName = null;
    this.newUser.email = null;
    this.newUser.login = null;
    this.newUser.password = null;
    this.newUser.firstName = null;
    this.repeatPasswordInput = null;
  }

  navigateToLogin() {
      this.router.navigate(['/login']);
  }

}
