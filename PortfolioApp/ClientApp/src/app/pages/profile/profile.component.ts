import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/User';
import { Router, ActivatedRoute } from '@angular/router';
import { Project } from 'src/app/models/Project';
import { PortfolioAppService } from 'src/app/services/portfolio-app.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
/** profile component*/
export class ProfileComponent implements OnInit {

  @Input() userInfo: User;
  userId: number;
  projectsList: Project[] = [];

  /** profile ctor */
  constructor(private route: ActivatedRoute, private portfolioAppService: PortfolioAppService, private router: Router) {

  }

  ngOnInit() {
    this.userId = this.route.snapshot.params['id'];
    this.getAllProjectsByUserId(this.userId);
  }

  onAddProject() {
    this.router.navigate(['/add-project', this.userId]);
  };

  getAllProjectsByUserId(id: number) {
    this.portfolioAppService.getAllProjectsByUserId(id).subscribe((data) => {
      this.projectsList = data;
      console.log(this.projectsList);
    },
      error => console.error(error)
    );
  }

}
