import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import { User } from 'src/app/models/User';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { PortfolioAppService } from 'src/app/services/portfolio-app.service';
import { RouterModule, Router } from '@angular/router';
import { NotifierService } from "angular-notifier";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

/** login component*/
export class LoginComponent implements OnInit {
  usersList: User[] = [];
  loginInput: string;
  passwordInput: string;
  loggingUser: User = new User;
  loggedUser: User;
  private readonly notifier: NotifierService;

  /** login ctor */
  constructor(private http: Http, private portfolioAppService: PortfolioAppService, private router: Router, notifierService: NotifierService) {
    this.notifier = notifierService;
  }

  ngOnInit(): void {
    console.log("on init works");
  }

 logIn() {
      this.portfolioAppService.logIn(this.loggingUser).subscribe((data) => {
      this.loggedUser = data;
      this.navigateToProfile(this.loggedUser.id);
    });
  }

  navigateToProfile(id: number) {
    this.router.navigate(['/profile', id]);
  }

}
