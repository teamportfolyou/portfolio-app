import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { User } from '../models/User';
import { Project } from '../models/Project';
import { Language } from '../models/Language';
import { Skill } from '../models/Skill';


@Injectable({
  providedIn: 'root'
})
export class PortfolioAppService {
  myAppUrl: string;
  myApiUsersUrl: string;
  myApiProjectUrl: string;
  userUrl: string;
  projectUrl: string;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    })
  };
  constructor(private http: HttpClient) {
    this.myAppUrl = environment.appUrl;
    this.myApiUsersUrl = 'api/Users';
    this.myApiProjectUrl = 'api/Projects';
    this.userUrl = this.myAppUrl + this.myApiUsersUrl;
    this.projectUrl = this.myAppUrl + this.myApiProjectUrl;

  }

  getLanguagesList(): Observable<Language[]> {
    return this.http.get<Language[]>(`/api/Languages/GetLanguagesList`, this.httpOptions);
  }

  logIn(user: User): Observable<User> {
    return this.http.post<User>(`/api/Users/CheckUser`, JSON.stringify(user), this.httpOptions);
  }

  registerUser(user): Observable<User> {
    return this.http.post<User>(this.userUrl, JSON.stringify(user), this.httpOptions);
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.userUrl, this.httpOptions);
  }

  addProject(project): Observable<Project> {
    return this.http.post<Project>(this.projectUrl, JSON.stringify(project), this.httpOptions);
  }

  getAllProjects(): Observable<Project[]> {
    return this.http.get<Project[]>(this.projectUrl, this.httpOptions);
  }

  getAllProjectsByUserId(userId: number): Observable<Project[]> {
    return this.http.get<Project[]>(`/api/Projects/User${userId}`, this.httpOptions);
  }

  checkEmailStatus(email: string): Observable<string> {
    return this.http.get<string>(`/api/Users/CheckEmailStatus_${email}`, this.httpOptions);
  }

  getSkillsList(): Observable<Skill[]> {
    return this.http.get<Skill[]>(`/api/Skills/GetSkillsList`, this.httpOptions);
  }


  errorHandler(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

}
