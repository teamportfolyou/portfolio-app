﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortfolioApp.Models
{
    public class Tag
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public int ProjectId { get; set; }
    }

}
