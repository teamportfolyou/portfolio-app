﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PortfolioApp.Models
{
    public class Project
    { 
        public int Id { get; set; } //tag table, language table, project table
        public string Name { get; set; } //project table
        public string Description { get; set; } //project table
        public virtual List<Tag> Tags { get; set; } //tag table
        public virtual List<Language> Languages { get; set; }  //language table
        public string Url { get; set; } //project table
        public int OwnerId { get; set; } //project table
        public DateTime PublishDate { get; set; } //project table
    }
}


